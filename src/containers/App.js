import React, {Component} from 'react';
import classes from './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {

    state = {
        persons: [
            {
                id: 'fdhjgk1',
                name: 'Sushil',
                age: 26
            },
            {
                id: 'sdfjw83',
                name: 'Shailesh',
                age: 24
            },
            {
                id: 'dfsh33',
                name: 'Pooja',
                age: 23
            }
        ],
        otherState: 'some other value',
        showPersons: false
    };

    nameChangeHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id;
        });

        const person = {
            ...this.state.persons[personIndex]
        }

        person.name = event.target.value;

        const persons = [...this.state.persons]
        persons[personIndex] = person;
        this.setState({persons: persons});
    };

    deletePersonHandler = (personIndex) => {
        // const persons = this.state.persons.splice();
        const persons = [...this.state.persons]
        persons.splice(personIndex, 1);
        this.setState({persons: persons});
    };

    togglePersonHandler = () => {

        const doesShow = this.state.showPersons;
        this.setState({showPersons: !doesShow});

    };

    render() {

        let persons = null;

        if (this.state.showPersons) {

            persons = <Persons
                persons={this.state.persons}
                clicked={this.deletePersonHandler}
                changed={this.nameChangeHandler}/>;
        }

        return (

            <div className={classes.App}>
                <Cockpit
                    showPersons={this.state.showPersons}
                    persons={this.state.persons}
                    clicked={this.togglePersonHandler}/>
                {persons}
            </div>

        );

        //return React.createElement('div',{className:'App'},React.createElement('h1',null,'Does this work now!!!'))
    }
}

export default App;
